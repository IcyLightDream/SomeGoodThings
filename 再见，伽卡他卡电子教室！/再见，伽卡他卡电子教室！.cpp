/*
   Copyright (c) 2021 2345Explorer
   SomeGoodThings is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.  
*/
//杀伽卡他卡电子教室用的 其他的可能不行 
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
int main(){
	printf("正在尝试杀掉进程...\n");
	system("taskkill /f /im Smonitor.exe");
	system("taskkill /f /im Student.exe");
	printf("正在添加打开管理器的注册表, 如果提示是否覆盖请输入Yes\n");
	system("reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v DisableTaskMgr /t REG_DWORD /d 0");
	printf("正在重启Explorer...\n");
	system("taskkill /f /im explorer.exe");
	ShellExecute(NULL,"open","Explorer.exe",NULL,NULL,SW_HIDE);
	return 0;
}

